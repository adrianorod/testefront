const gulp = require('gulp');
const imageMin = require('gulp-imagemin');
const cache = require('gulp-cache');
const config = require('./config');

gulp.task('images', function() {
	return gulp.src(config.path.img.src)
		.pipe(cache(imageMin()))
        .pipe(gulp.dest(config.path.img.dest));
});
