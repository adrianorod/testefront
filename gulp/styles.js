const gulp = require('gulp');
const plumber = require('gulp-plumber');
const scss = require('gulp-sass');
const wait = require('gulp-wait');
const concat = require('gulp-concat');
const csslint = require('gulp-csslint');
const cssnano = require('gulp-cssnano');
const sourcemaps = require('gulp-sourcemaps');
const config = require('./config');

gulp.task('cssLib', function() {
    return gulp.src(config.path.scss.libs)
        .pipe(concat('libs.css'))
        .pipe(gulp.dest(config.path.scss.dest));
});

gulp.task('css', ['cssLib'], function() {
    return gulp.src(config.path.scss.src)
        .pipe(wait(30))
        .pipe(plumber())
        .pipe(scss())
		.pipe(csslint({
			'font-faces': false,
			'box-model': false,
			'unique-headings': false
		}))
		.pipe(csslint.formatter())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(config.path.scss.dest));
});

gulp.task('cssMinify', ['cssLib'], function() {
    return gulp.src(config.path.scss.src)
        .pipe(wait(30))
        .pipe(plumber())
        .pipe(scss())
        .pipe(csslint({
            'font-faces': false,
            'box-model': false,
            'unique-headings': false
        }))
        .pipe(csslint.formatter())
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.init())
        .pipe(cssnano({
            discardUnused: false,
            autoprefixer: false
        }))
        .pipe(sourcemaps.write('./map'))
        .pipe(gulp.dest(config.path.scss.dest));
});
