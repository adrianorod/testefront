const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const config = require('./config');


gulp.task('jsLib', function() {
    return gulp.src(config.path.js.libs)
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(concat('libs.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.path.js.dest));
});

gulp.task('js', ['jsLib'], function() {
    return gulp.src(config.path.js.src)
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(config.path.js.dest));
});

gulp.task('jsMinify', ['jsLib'], function() {
    return gulp.src(config.path.js.src)
        .pipe(babel({ presets: ['es2015'] }))
        .pipe(concat('scripts.js'))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./map'))
        .pipe(gulp.dest(config.path.js.dest));
});
