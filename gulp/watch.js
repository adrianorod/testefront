var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('./config');

gulp.task('watch', function() {
    gulp.watch([config.path.scss.src, './dev/assets/scss/**/*.scss'], ['css']).on('change', browserSync.reload);
    gulp.watch(config.path.js.src, ['js']).on('change', browserSync.reload);
    gulp.watch(config.path.html.src, ['html']).on('change', browserSync.reload);
    gulp.watch(config.path.img.src, ['images']).on('change', browserSync.reload);
});

gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: 'dist'
        }
    });
});
