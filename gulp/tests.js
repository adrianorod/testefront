const gulp = require('gulp');
const karma = require('gulp-karma');
const config = require('./config');

const files = [].concat(config.path.js.libs, ['./node_modules/angular-mocks/angular-mocks.js'], config.path.js.src);

gulp.task('test:unit', function(coverage) {
    gulp.src(files)
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function(err) {
            throw err;
        });
});
