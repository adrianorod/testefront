const path = {
    global: {
        dev: './dev',
        dist: './dist'
    },

    html: {
        src: [
            './dev/**/*.html'
        ],
        dest: './dist'
    },

    img: {
        src: './dev/assets/img/**/*',
        dest: './dist/assets/img'
    },

    scss: {
        libs: [
            './node_modules/bootstrap/dist/css/bootstrap.min.css'
        ],
        src: [
            './dev/assets/scss/main.scss',
            './dev/components/**/*.scss'
        ],
        dest: './dist/assets/css'
    },

    js: {
        libs: [
            './node_modules/babel-polyfill/dist/polyfill.min.js',
            './node_modules/angular/angular.min.js',
            './node_modules/angular-route/angular-route.min.js',
            './node_modules/angular-animate/angular-animate.min.js'
        ],
        src: [
            './dev/app.js',
            './dev/app.config.js',
            './dev/app.controller.js',
            './dev/utils/**/*.js',
            './dev/services/**/*.js',
            './dev/components/**/*.js'
        ],
        dest: './dist/assets/js',
    }
};

module.exports.path = path;
