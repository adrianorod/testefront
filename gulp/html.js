const gulp = require('gulp');
const config = require('./config');

gulp.task('html', function () {
    return gulp.src(config.path.html.src)
        .pipe(gulp.dest(config.path.html.dest));
});
