const gulp = require('gulp');
const requireDir = require('require-dir');
const runSequence = require('run-sequence');

requireDir('gulp');

gulp.task('dev', function () {
	return runSequence(
		'clean',
		'css',
		'js',
		'html',
		'images',
		'watch',
		'server'
	);
});

gulp.task('build', function () {
	return runSequence(
		'clean',
		'cssMinify',
		'jsMinify',
		'html',
		'images'
	);
});
