(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.service('planosService', planosService);

	planosService.$inject = ['$http', 'Constants'];

	function planosService($http, Constants) {

        const vm = this;
        vm.getPlanos = getPlanos;

		function getPlanos(id) {
			return $http.get(`${Constants.getPlanos}/${id}`)
                .then(data => data.data)
                .then(data => data.planos);
		}

	}

})();