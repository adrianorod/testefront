(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.service('plataformasService', plataformasService);

	plataformasService.$inject = ['$http', 'Constants'];

	function plataformasService($http, Constants) {

        const vm = this;
        vm.getPlataformas = getPlataformas;

		function getPlataformas() {
			return $http.get(Constants.getPlataformas)
                .then(data => data.data)
                .then(data => data.plataformas);
		}

	}

})();