(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.controller('appController', appController);

	appController.$inject = ['globalDataFactory'];

	function appController(globalDataFactory) {

		const vm = this;
		vm.globalData = globalDataFactory;
		vm.globalData.goBack = false;

		init();

		function init() { }

	}

})();
