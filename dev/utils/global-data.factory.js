(function () {
    'use strict';
  
    angular
        .module('CelularDireto')
        .service('globalDataFactory', globalDataFactory);
  
    globalDataFactory.$inject = [];
  
    function globalDataFactory() {
  
      this.data = {};
  
    }
  
  })();
  