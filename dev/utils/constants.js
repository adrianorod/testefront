(function () {
	'use strict';

    var urlApiBase = 'http://private-59658d-celulardireto2017.apiary-mock.com';

	angular
		.module('CelularDireto')
		.constant('Constants', {
            getPlataformas: `${urlApiBase}/plataformas`,
            getPlanos: `${urlApiBase}/planos`
		});
})();
