(function () {
    'use strict';

    angular
        .module('CelularDireto')
        .config(ConfigProvider)

    ConfigProvider.$inject = [
        '$routeProvider'
    ];

    function ConfigProvider($routeProvider) {

        $routeProvider
            .when('/plataformas', {
                template: '<plataformas></plataformas>'
            })
            .when('/planos/:id', {
                template: '<planos></planos>'
            })
            .when('/cadastro', {
                template: '<cadastro></cadastro>'
            })
            .otherwise('/plataformas');
    }
})();
