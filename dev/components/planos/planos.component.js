(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.controller('planosController', planosController)
		.component('planos', {
			templateUrl: 'components/planos/planos.component.html',
			controller: planosController,
			controllerAs: 'vm'
		});

	planosController.$inject = ['planosService', 'globalDataFactory', '$routeParams', '$location'];

	function planosController(planosService, globalDataFactory, $routeParams, $location) {

		const vm = this;
        vm.globalData = globalDataFactory;
		vm.globalData.goBack = goBack;
		vm.planos = {};
		vm.setPlano = setPlano;

		init();

		function init() {
			checkData();
			getPlanos($routeParams.id);
		}

		function checkData() {
			if (!vm.globalData.plataforma) goBack();
		}

		function getPlanos(id) {
			planosService.getPlanos(id)
				.then(data => vm.planos = data)
				.catch(error => {
					console.log(error);
					goBack();
				});
		}

		function setPlano(plano) {
			vm.globalData.plano = plano;
			$location.path('/cadastro');
		}

		function goBack() {
			$location.path('/plataformas');
			vm.globalData.goBack = false;
		}

	}

})();
