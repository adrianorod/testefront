(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.controller('cadastroController', cadastroController)
		.component('cadastro', {
			templateUrl: 'components/cadastro/cadastro.component.html',
			controller: cadastroController,
			controllerAs: 'vm'
		});

	cadastroController.$inject = ['globalDataFactory', '$location'];

	function cadastroController(globalDataFactory, $location) {

		const vm = this;
		vm.globalData = globalDataFactory;
		vm.globalData.goBack = goBack;
		vm.cadastro = {};
		vm.cadastroRealizado = false;
		vm.sendData = sendData;

		init();

		function init() {
			checkData();
		}

		function checkData() {
			if (!vm.globalData.plataforma || !vm.globalData.plano) {
				$location.path('/plataformas');
				vm.globalData.goBack = false;
			}
		}

		function goBack() {
			$location.path(`/planos/${vm.globalData.plataforma.sku}`);
		}

		function sendData() {
			console.log('-------------------------');
			console.log('=== PLATAFORMA SELECIONADA ===');
			console.log(vm.globalData.plataforma);
			console.log('-------------------------');
			console.log('=== PLANO SELECIONADO ===');
			console.log(vm.globalData.plano);
			console.log('-------------------------')
			console.log('=== DADOS DE CADASTRO ===');
			console.log(vm.cadastro);
			console.log('-------------------------');
			vm.cadastroRealizado = true;
			vm.globalData.goBack = false;
		}

	}

})();
