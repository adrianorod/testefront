(function () {
	'use strict';

	angular
		.module('CelularDireto')
		.controller('plataformasController', plataformasController)
		.component('plataformas', {
			templateUrl: 'components/plataformas/plataformas.component.html',
			controller: plataformasController,
			controllerAs: 'vm'
		});

	plataformasController.$inject = ['plataformasService', 'globalDataFactory', '$location'];

	function plataformasController(plataformasService, globalDataFactory, $location) {

		const vm = this;
        vm.globalData = globalDataFactory;
		vm.plataformas = {};
		vm.getPlataformas = getPlataformas;
		vm.setPlataforma = setPlataforma;

		init();

		function init() {
			getPlataformas();
		}

		function getPlataformas() {
			return plataformasService.getPlataformas()
				.then(data => vm.plataformas = data)
				.catch(error => console.log(error));
		}

		function setPlataforma(plataforma) {
			vm.globalData.plataforma = plataforma;
			$location.path(`/planos/${plataforma.sku}`);
		}

	}

})();
