describe('Controller: plataformasController', function() {
    beforeEach(module('CelularDireto'));
  
    var plataformasController = {};
  
    beforeEach(inject(function($controller) {
        // scope = {};
        plataformasController = $controller('plataformasController', {});
    }));

    it('deve possuir propriedade chamada "plataformas"', function() {
        expect(plataformasController.plataformas).toBeDefined();
    });
});
