# TesteFront

Candidato: Adriano Azevedo Rodrigues (adrianoazevedo.r@gmail.com)

O projeto foi executado utilizando Angularjs, Gulp e Sass. A arquitetura e código seguem o padrão sugerido por John Papa, utilizando components e services.

## Dependências

1. Dependências globais

É necessário ter o NodeJs, o Gulp e o Sass instalados globalmente na máquina. Caso não tenha, instale o NodeJs através do site oficial e execute os seguintes comandos no terminal para instalação dos demais:

`npm install gulp -g`
`npm install sass -g`

2. Dependências internas

Todas as dependências do projeto podem ser instaladas com um único comando: `npm install`

## Executando localmente

Após a instalação das dependências, você poderá executar o projeto localmente com o seguinte comando:

`gulp dev`

## Gerando build

Para publicação do projeto em algum ambiente de homologação ou produção, utilizamos o comando:

`gulp build`